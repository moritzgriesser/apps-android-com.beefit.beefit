package com.beefit.beefit.registration_slider;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.beefit.beefit.R;

import java.util.ArrayList;

public class MyProfessionFragment extends Fragment {

    private ArrayList<RadioButton> radioButtons;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_my_profession, container, false);

        radioButtons = new ArrayList<>();
        radioButtons.add((RadioButton) view.findViewById(R.id.rb_1));
        radioButtons.add((RadioButton) view.findViewById(R.id.rb_2));
        radioButtons.add((RadioButton) view.findViewById(R.id.rb_3));

        for(RadioButton r : radioButtons) {
            r.setButtonTintList(
                    new ColorStateList(
                            new int[][]{
                                    new int[]{android.R.attr.state_checked}, //checked
                                    new int[]{-android.R.attr.state_checked} //unchecked
                            },
                            new int[] {
                                    getContext().getColor(R.color.colorPrimary),
                                    getContext().getColor(R.color.colorAccent)

                            }
                    )
            );
            r.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(RadioButton radioButton : radioButtons) {

                        if(radioButton.equals(v)) {
                            radioButton.setChecked(true);
                        }else {
                            radioButton.setChecked(false);
                        }
                    }
                }
            });
        }

        return view;
    }
}
