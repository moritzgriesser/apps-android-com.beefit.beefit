package com.beefit.beefit.opendata;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ConnectOpenData {

    private OkHttpClient okHttpClient;

    public ConnectOpenData(Callback callback) throws IOException {
        okHttpClient = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://ipchannels.integreen-life.bz.it/meteorology/rest/get-station-details")
                .build();

        okHttpClient.newCall(request).enqueue(callback);
    }

    public void getAccessToken(){
        Request request = new Request.Builder()
                .url("https://ipchannels.integreen-life.bz.it/meteorology/rest/access-token")
                .addHeader("Authorization", "Bearer my_token")
                .build();



        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Error");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                System.out.println(response.body().string());
            }
        });
    }
}
