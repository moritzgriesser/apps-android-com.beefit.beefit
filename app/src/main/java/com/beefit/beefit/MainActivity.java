package com.beefit.beefit;

import android.support.design.button.MaterialButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.beefit.beefit.registration_slider.GoalFragment;
import com.beefit.beefit.registration_slider.MyProfessionFragment;
import com.beefit.beefit.registration_slider.PersonalDataFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TextView tv_back;
    MaterialButton bt_next;
    private ViewPager vp_registration;
    private PagerAdapter vp_registration_adapter;
    private ArrayList<Fragment> pagerItems = new ArrayList<>();
    private int[] buttonIds = {R.id.bt_1, R.id.bt_2, R.id.bt_3};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bt_next = findViewById(R.id.bt_next);

        tv_back = findViewById(R.id.tv_back);

        vp_registration = findViewById(R.id.vp_registration);

        vp_registration_adapter = new VpRegistrationAdapter(getSupportFragmentManager());
        vp_registration.setAdapter(vp_registration_adapter);

        vp_registration.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setButtonState(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp_registration.setCurrentItem(vp_registration.getCurrentItem() - 1);
            }
        });

        setButtonState(0);
    }

    private void setButtonState(int position) {
        int counter = 0;

        if (position == 0) {
            tv_back.setVisibility(View.GONE);

        } else if (position == pagerItems.size() - 1) {
            bt_next.setVisibility(View.GONE);
            tv_back.setVisibility(View.VISIBLE);

        } else {
            bt_next.setVisibility(View.VISIBLE);
            tv_back.setVisibility(View.VISIBLE);
        }

        for (int i : buttonIds) {
            Button button = findViewById(i);

            if (counter == position) {
                button.setBackground(ContextCompat.getDrawable(
                        MainActivity.this, R.drawable.page_item_active
                ));
            } else {
                button.setBackground(
                        ContextCompat.getDrawable(
                                MainActivity.this, R.drawable.page_item_inactive));
            }

            counter++;
        }
    }

    public void nextButtonClick(View view) {
        vp_registration.setCurrentItem(vp_registration.getCurrentItem() + 1);
    }


    private class VpRegistrationAdapter extends FragmentStatePagerAdapter {

        private VpRegistrationAdapter(FragmentManager fm) {
            super(fm);

            pagerItems.add(new PersonalDataFragment());
            pagerItems.add(new MyProfessionFragment());
            pagerItems.add(new GoalFragment());
        }

        @Override
        public Fragment getItem(int position) {
            return pagerItems.get(position);
        }


        @Override
        public int getCount() {
            return pagerItems.size();
        }
    }
}
