package com.beefit.beefit;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.beefit.beefit.opendata.ConnectOpenData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.security.PrivateKey;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class LocationActivity extends AppCompatActivity implements LocationListener {

    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ContextCompat.checkSelfPermission(LocationActivity.this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(LocationActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

            } else {
                ActivityCompat.requestPermissions(LocationActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);

            }
        } else {

            getLocation();
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        SharedPreferences sharedPreferences = getSharedPreferences("locations", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat("lat", (float) location.getLatitude());
        editor.putFloat("lng", (float) location.getLongitude());

        editor.apply();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 123: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getLocation();

                } else {
                    //TODO: handle abort


                }
                return;
            }
        }
    }

    private void getLocation() {
        onLocationChanged(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5, 0, this);

        try {
            ConnectOpenData connectOpenData = new ConnectOpenData(
                    new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            System.out.println(e.getMessage());
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            try {
                                JSONArray result = new JSONArray(response.body().string());

                                double distance_lat_long = Integer.MAX_VALUE;
                                JSONObject nearestStation = result.getJSONObject(0);

                                SharedPreferences sharedPreferences =
                                        getSharedPreferences("locations", Context.MODE_PRIVATE);

                                double savedlat = sharedPreferences.getFloat("lat", 2.0f);
                                double savedlng = sharedPreferences.getFloat("lng", 2.0f);

                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);

                                    double lat = item.getDouble("latitude");
                                    double lng = item.getDouble("longitude");

                                    double tempdistance = Math.abs(lat - savedlat) + Math.abs(lng - savedlng);

                                    if (tempdistance < distance_lat_long) {
                                        distance_lat_long = tempdistance;
                                        nearestStation = item;
                                    }

                                }
                                final JSONObject finalNearestStation = nearestStation;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        TextView tv_nearest_weather_station = findViewById(R.id.tv_nearest_weather_station);
                                        try {
                                            tv_nearest_weather_station.setText(finalNearestStation.getString("name"));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
            );

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
